package com.duytd.daggerexample.component;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.duytd.daggerexample.DemoApplication;
import com.duytd.daggerexample.helper.DataManager;
import com.duytd.daggerexample.helper.DbHelper;
import com.duytd.daggerexample.injection.ApplicationContext;
import com.duytd.daggerexample.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface  ApplicationComponent {
    void inject(DemoApplication demoApplication);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    DataManager getDataManager();

    SharedPreferences getSharedPreferencesHelper();

    DbHelper getDeHelper();
}
