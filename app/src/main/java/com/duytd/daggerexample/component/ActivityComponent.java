package com.duytd.daggerexample.component;

import com.duytd.daggerexample.MainActivity;
import com.duytd.daggerexample.injection.PerActivity;
import com.duytd.daggerexample.module.ActivityModule;

import dagger.Component;
import dagger.Subcomponent;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)

public interface ActivityComponent {
    void inject(MainActivity mainActivity);

}
