package com.duytd.daggerexample;

import android.app.Application;
import android.content.Context;

import com.duytd.daggerexample.component.ApplicationComponent;
import com.duytd.daggerexample.component.DaggerApplicationComponent;
import com.duytd.daggerexample.helper.DataManager;
import com.duytd.daggerexample.module.ApplicationModule;

import javax.inject.Inject;

public class DemoApplication extends Application {

    protected ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    public static DemoApplication get(Context context){
        return (DemoApplication)context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getComponent(){
        return applicationComponent;
    }
}
