package com.duytd.daggerexample.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

//@Qualifier: Dùng để xác định đầy đủ, đôi khi mình sử dụng Application Context và Activity Context cả 2 điều sử dụng Context.

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityContext {
}
