package com.duytd.daggerexample.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

//@Scope: được sử dụng để xác định phạm vi trong đó một đối tượng phụ thuộc vẫn tồn tại.

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
